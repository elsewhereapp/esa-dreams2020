package com.elsewhere.esa.dreams2020.comparator;

import com.dreamcloud.esa_core.similarity.SimilarityInfo;
import com.dreamcloud.esa_core.similarity.TextSimilarity;
import com.dreamcloud.esa_core.similarity.TopConcept;
import com.dreamcloud.esa_score.score.DocumentNameResolver;
import com.elsewhere.esa.dreams2020.DayByDayComparisonRecord;
import com.elsewhere.esa.dreams2020.dataset.DataRecord;
import com.elsewhere.esa.dreams2020.dataset.DataSet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.tongfei.progressbar.ProgressBar;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static java.time.temporal.ChronoUnit.DAYS;

public class DayByDayComparator {
    private static final float HIGH_THRESHOLD = 0.175f;
    private static final float INDICATIVE_THRESHOLD = 0.07f;
    private static final float LOW_THRESHOLD = 0.05f;

    private final TextSimilarity similarity;
    private final DataSet dreamSet;
    private final DataSet newsSet;

    private final File outputFolder;

    private final DocumentNameResolver nameResolver;

    private final AtomicInteger daysProcessed = new AtomicInteger(0);


    public DayByDayComparator(TextSimilarity similarity, DataSet dreamSet, DataSet newsSet, File outputFolder, DocumentNameResolver nameResolver) throws IOException {
        this.similarity = similarity;
        this.dreamSet = dreamSet;
        this.newsSet = newsSet;
        this.outputFolder = outputFolder;
        this.nameResolver = nameResolver;
    }

    public void compare(int year, int startMonth, int startDay, int endMonth, int endDay, int range, int topConceptsPerDay) throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(20);

        LocalDate startDate = LocalDate.of(year, startMonth, startDay);
        LocalDate endDate = LocalDate.of(year, endMonth, endDay);
        long numDays = DAYS.between(startDate, endDate) + 1;
        System.out.println("Number of days: " + numDays);

        while (!startDate.isAfter(endDate.plusDays(1))) {
            //Skip if the file already exists
            File dayOutputFile = new File(outputFolder, "day-" + startDate + ".json");
            if (dayOutputFile.exists()) {
                System.out.println("Skipping " + startDate + " because it has already been processed.");
                startDate = startDate.plusDays(1);
                continue;
            }

            //Find the dreams on this day
            DataSet currentDaySet = new DataSet(null);
            for (DataRecord dreamSetRecord: dreamSet.records) {
                LocalDate dreamDate = dreamSetRecord.date.withYear(year);
                if (dreamDate.equals(startDate)) {
                    currentDaySet.records.add(dreamSetRecord);
                }
            }

            System.out.println("Found " + currentDaySet.records.size() + " dream records on " + startDate);

            //Find all of the set2 records within a month of this day
            int monthRecords = 0;
            Map<String, DataSet> newsMap = new HashMap<>();
            for (DataRecord newsSetRecord: newsSet.records) {
                LocalDate newsDate = newsSetRecord.date.withYear(year);
                if (newsDate.isAfter(startDate.minusDays(range)) && newsDate.isBefore(startDate.plusDays(range))) {
                    String newsDay = String.valueOf(newsDate.getDayOfMonth());
                    String newsMonth = String.valueOf(newsDate.getMonthValue());
                    if (newsDay.length() == 1) {
                        newsDay = "0" + newsDay;
                    }
                    if (newsMonth.length() == 1) {
                        newsMonth = "0" + newsMonth;
                    }

                    String dayKey = newsMonth + "-" + newsDay;
                    if (!newsMap.containsKey(dayKey)) {
                        newsMap.put(dayKey, new DataSet(null));
                    }
                    DataSet currentMonthSet = newsMap.get(dayKey);
                    currentMonthSet.records.add(newsSetRecord);
                    monthRecords++;
                }
            }

            System.out.println("Found " + monthRecords + " news records within " + range + " days of " + startDate);

            //Spin up a thread to handle this comparison
            LocalDate date = LocalDate.from(startDate);
            System.out.println("Queueing thread for " + date);
            executor.execute(() -> {
                try {
                    this.compareDay(date, currentDaySet, newsMap, topConceptsPerDay);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            startDate = startDate.plusDays(1);
        }

        System.out.println("All threads started...");

        try (ProgressBar pb = new ProgressBar("Dreams 2020", numDays)) {
            while (true) {
                int days = this.daysProcessed.get();

                if (days != pb.getCurrent()) {
                    pb.stepTo(days);
                }

                if (days >= numDays) {
                    System.out.println("progress bar finished.");
                    break;
                }

                Thread.sleep(5000);
            }
        }

        System.out.println("Processed all " + numDays + " days of dreams.");

        executor.shutdown();
        executor.awaitTermination(5, TimeUnit.MINUTES);
    }

    public void compareDay(LocalDate date, DataSet dayDreamSet, Map<String, DataSet> newsMap, int topConceptsPerDay) throws Exception {
        System.out.println("Thread started for " + date);

        //Sort news map keys (dates) to get the earliest and latest date
        List<String> newsMapKeys = new ArrayList<>(newsMap.keySet());
        Collections.sort(newsMapKeys);
        String newsDateRange = newsMapKeys.get(0) + "-" + newsMapKeys.get(newsMapKeys.size() - 1);

        //Format the date
        String dreamDay = String.valueOf(date.getDayOfMonth());
        String dreamMonth = String.valueOf(date.getMonthValue());
        if (dreamDay.length() == 1) {
            dreamDay = "0" + dreamDay;
        }
        if (dreamMonth.length() == 1) {
            dreamMonth = "0" + dreamMonth;
        }

        DayByDayComparisonSet comparisonSet = new DayByDayComparisonSet(dreamSet.name, dreamMonth + "-" + dreamDay, dayDreamSet.records.size(), newsSet.name, newsDateRange);

        int newsRecords = 0;
        //Compare dreams on the current day to news on each day within the range
        for (String dayKey: newsMap.keySet()) {
            System.out.println("Comparing " + date + " to " + dayKey + "...");

            //Keep track of top concept scores for each day
            Map<String, Float> topConceptScores = new HashMap<>();

            int wordCount = 0;
            float totalDayScore = 0;
            SimilarityInfo high = new SimilarityInfo(0);
            int highCount = 0;
            SimilarityInfo indicative = new SimilarityInfo(0);
            int indicativeCount = 0;
            SimilarityInfo low = new SimilarityInfo(0);
            int lowCount = 0;
            SimilarityInfo indiscernible = new SimilarityInfo(1);
            int indiscernibleCount = 0;

            DataSet periodNews = newsMap.get(dayKey);
            newsRecords += periodNews.records.size();

            for (int setIdx = 0; setIdx < dayDreamSet.records.size(); setIdx++) {
                DataRecord dayRecord = dayDreamSet.records.get(setIdx);
                for (DataRecord newsRecord: periodNews.records) {
                    SimilarityInfo similarityInfo =  similarity.score(dayRecord.comparisonText, newsRecord.comparisonText, topConceptsPerDay);

                    //Get a word count
                    wordCount += dayRecord.comparisonText.split(" ").length;
                    wordCount += newsRecord.comparisonText.split(" ").length;

                    float score = similarityInfo.getScore();
                    totalDayScore += score;

                    similarityInfo.setDoc1Id(String.valueOf(dayRecord.id));
                    similarityInfo.setDoc2Id(String.valueOf(newsRecord.id));

                    if (score > HIGH_THRESHOLD) {
                        highCount++;
                        if (score > high.getScore()) {
                            high = similarityInfo;
                        }
                    } else if (score > INDICATIVE_THRESHOLD) {
                        indicativeCount++;
                        indicative = similarityInfo;
                    } else if (score > LOW_THRESHOLD) {
                        lowCount++;
                        low = similarityInfo;
                    } else {
                        indiscernibleCount++;
                        if (score < indiscernible.getScore()) {
                            indiscernible = similarityInfo;
                        }
                    }

                    //Resolve names
                    for (TopConcept topConcept: similarityInfo.getTopConcepts()) {
                        String resolved = nameResolver.getTitle(Integer.parseInt(topConcept.getConcept()));
                        resolved = "https://en.wikipedia.org/wiki/" + resolved.substring(0, 1).toUpperCase() + resolved.substring(1);;
                        topConcept.setConcept(resolved);
                    }

                    for (TopConcept topConcept: similarityInfo.getTopConcepts()) {
                        String concept = topConcept.getConcept();
                        if (!topConceptScores.containsKey(concept)) {
                            topConceptScores.put(concept, topConcept.getScore());
                        } else {
                            float currentScore = topConceptScores.get(concept);
                            topConceptScores.put(concept, currentScore + topConcept.getScore());
                        }
                    }
                }
            }

            totalDayScore /= (dayDreamSet.records.size() * periodNews.records.size());

            //Sort the top concepts for the day and keep the top N
            Vector<TopConcept> topConcepts = new Vector<>();
            for (String concept: topConceptScores.keySet()) {
                topConcepts.add(new TopConcept(concept, topConceptScores.get(concept)));
            }

            topConcepts.sort((t1, t2) -> Float.compare(t2.getScore(), t1.getScore()));
            topConcepts.setSize(topConceptsPerDay);

            //Add the day to the comparison set
            ArrayList<SimilarityInfo> exampleComparisons = new ArrayList<>();
            exampleComparisons.add(high);
            exampleComparisons.add(indicative);
            exampleComparisons.add(low);
            exampleComparisons.add(indiscernible);

            DayByDayComparisonRecord record = new DayByDayComparisonRecord(dayKey, periodNews.records.size(), totalDayScore, wordCount, topConcepts.subList(0, topConcepts.size()), exampleComparisons);
            record.highScores = highCount;
            record.indicativeScores = indicativeCount;
            record.lowScores = lowCount;
            record.indiscernibleScores = indiscernibleCount;
            comparisonSet.addNewsRecord(record);
        }

        System.out.println("Writing comparison set with " + comparisonSet.newsRecords.size() + " records...");
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        FileWriter writer = new FileWriter(new File(outputFolder, "day-" + date + ".json"));
        gson.toJson(comparisonSet, writer);
        writer.close();
        System.out.println("Thread finished for " + date);

        this.daysProcessed.incrementAndGet();
    }
}
