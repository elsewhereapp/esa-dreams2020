package com.elsewhere.esa.dreams2020.comparator;

import com.dreamcloud.esa_core.similarity.TextSimilarity;
import com.elsewhere.esa.dreams2020.dataset.DataRecord;
import com.elsewhere.esa.dreams2020.dataset.DataSet;
import com.opencsv.CSVWriter;
import me.tongfei.progressbar.ProgressBar;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class RoundRobinComparator {
    private TextSimilarity similarity;
    AtomicInteger recordsWritten = new AtomicInteger(0);

    public RoundRobinComparator(TextSimilarity similarity) {
        this.similarity = similarity;
    }

    public void compare(DataSet dataSet1, DataSet dataSet2, File outputFolder) throws Exception {
        int comparisonCount = dataSet1.records.size() * dataSet2.records.size();

        int threadCount = 20;
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
        ArrayList<Runnable> processors = new ArrayList<>();

        int recordsPerThread = dataSet1.records.size() / threadCount;
        int recordsLeftOver = dataSet1.records.size() % threadCount;
        for (int threadIdx = 0; threadIdx < threadCount; threadIdx++) {
            int offset = threadIdx * recordsPerThread;
            int count = recordsPerThread;
            if (threadIdx == threadCount - 1) {
                count += recordsLeftOver;
            }
            int finalOffset = offset;
            int finalCount = count;
            processors.add(() -> {
                try {
                    this.compareRecords(dataSet1, dataSet2, finalOffset, finalCount, outputFolder);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
        }

        //Ensure that all of the news articles are cached (multi-threading causes a lot of unnecessary repeats)
        for (DataRecord set2Record: dataSet2.records) {
            similarity.score("", set2Record.comparisonText);
            if (set2Record.id % 100 == 0) {
                System.out.println("Processing record " + set2Record.id);
            }
        }

        System.out.println("Warmed up caches.");

        //Spin up some threads!
        for (Runnable processor: processors) {
            executorService.execute(processor);
        }

        System.out.println("Starting the threads...");

        try (ProgressBar pb = new ProgressBar("Dreams 2020", comparisonCount)) {
            while (true) {
                int recordsWritten = this.recordsWritten.get();

                pb.stepTo(this.recordsWritten.get());

                if (recordsWritten == comparisonCount) {
                    break;
                }

                Thread.sleep(1000);
            }
        }
    }

    private void compareRecords(DataSet set1, DataSet set2, int offset, int count, File outputFolder) throws Exception {
        System.out.println("Starting thread to handle records from " + offset + " to " + (offset + count) + "...");
        String fileName = String.valueOf(offset);
        if (offset < 10) {
            fileName = "0" + fileName;
        }
        fileName = outputFolder + "/" + "comparison_pairs_from_" + fileName + "_to_" + (offset + count) + ".csv";

        Writer fileWriter = new FileWriter(fileName);
        CSVWriter writer = new CSVWriter(fileWriter);

        for (int setIdx = offset; setIdx < offset + count; setIdx++) {
            DataRecord set1Record = set1.records.get(setIdx);
            for (DataRecord set2Record: set2.records) {
                float score = similarity.score(set1Record.comparisonText, set2Record.comparisonText).getScore();
                if (Float.isNaN(score)) {
                    System.out.println("Found a NaN while processing " + set1Record.id + "-" + set2Record.id);
                    System.out.println("-------------------");
                    System.out.println(set1Record.comparisonText);
                    System.out.println("-------------------");
                    System.out.println(set2Record.comparisonText);
                    System.out.println("-------------------");
                    throw new RuntimeException("NaN!");
                }
                writer.writeNext(new String[]{String.valueOf(set1Record.id), String.valueOf(set2Record.id), String.valueOf(score)});
                this.recordsWritten.incrementAndGet();
            }
        }

        System.out.println("Thread completed.");

        writer.close();
    }
}
