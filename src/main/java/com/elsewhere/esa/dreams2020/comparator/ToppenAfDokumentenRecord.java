package com.elsewhere.esa.dreams2020.comparator;

import com.dreamcloud.esa_core.similarity.TopConcept;

import java.util.List;

public class ToppenAfDokumentenRecord {
    public String date;
    public String text;
    public float similarity;
    public List<TopConcept> topConcepts;
}
