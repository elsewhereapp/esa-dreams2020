package com.elsewhere.esa.dreams2020.comparator;

import com.dreamcloud.esa_core.similarity.SimilarityInfo;
import com.dreamcloud.esa_core.similarity.TextSimilarity;
import com.dreamcloud.esa_core.similarity.TopConcept;
import com.dreamcloud.esa_score.score.DocumentNameResolver;
import com.elsewhere.esa.dreams2020.dataset.DataRecord;
import com.elsewhere.esa.dreams2020.dataset.DataSet;
import me.tongfei.progressbar.ProgressBar;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ToppenAfDokumenten {
    private final TextSimilarity similarity;

    private final DataSet dreamSet;
    private final DataSet newsSet;

    private final DocumentNameResolver nameResolver;

    public ToppenAfDokumenten(TextSimilarity textSimilarity, DataSet dreamSet, DataSet newsSet, DocumentNameResolver nameResolver) {
        this.similarity = textSimilarity;
        this.dreamSet = dreamSet;
        this.newsSet = newsSet;
        this.nameResolver = nameResolver;
    }

    public DocumentNameResolver getNameResolver() {
        return nameResolver;
    }

    private List<SimilarityInfo> getMostSimilarFromDataSet(String text, int records, int concepts, DataSet dataSet) throws Exception {
        List<SimilarityInfo> mostSimilar = new ArrayList<>(records);
        for (int i = 0; i < records; i++) {
            mostSimilar.add(new SimilarityInfo(0));
        }

        int comparisonCount = dataSet.records.size();
        ProgressBar progressBar = new ProgressBar("Top Document", comparisonCount);

        int threadCount = 20;
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
        ArrayList<Callable<List<SimilarityInfo>>> processors = new ArrayList<>();

        int recordsPerThread = comparisonCount / threadCount;
        int recordsLeftOver = comparisonCount % threadCount;
        for (int threadIdx = 0; threadIdx < threadCount; threadIdx++) {
            int offset = threadIdx * recordsPerThread;
            int count = recordsPerThread;
            if (threadIdx == threadCount - 1) {
                count += recordsLeftOver;
            }
            int finalOffset = offset;
            int finalCount = count;
            processors.add(() -> {
                try {
                    return this.compareRecords(text, dataSet, records, concepts, finalOffset, finalCount, mostSimilar, progressBar);
                } catch (Exception e) {
                    System.out.println("thread exception: " + e.getMessage());
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            });
        }

        //Spin up some threads!
        System.out.println("Starting the threads...");
        List<Future<List<SimilarityInfo>>> futures = executorService.invokeAll(processors);
        for (Future<List<SimilarityInfo>> future : futures) {
            try {
                future.get();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        System.out.println("Threads finished.");
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.MINUTES);

        //Resolve top concepts
        for (SimilarityInfo info : mostSimilar) {
            for (TopConcept concept : info.topConcepts) {
                concept.setConcept(this.nameResolver.getTitle(Integer.parseInt(concept.getConcept())));
            }
        }

        return mostSimilar;
    }

    private List<SimilarityInfo> compareRecords(String text, DataSet dataSet, int records, int topConcepts, int offset, int count, List<SimilarityInfo> mostSimilar, ProgressBar progressBar) throws Exception {
        for (int i = offset; i < offset + count; i++) {
            DataRecord dreamRecord = dataSet.records.get(i);
            SimilarityInfo recordSimilarity = similarity.score(text, dreamRecord.comparisonText, topConcepts);
            recordSimilarity.setDoc1Id(text);
            recordSimilarity.setDoc2Id(String.valueOf(dreamRecord.id));
            synchronized (mostSimilar) {
                SimilarityInfo leastSimilar = mostSimilar.get(records - 1);
                if (recordSimilarity.getScore() > leastSimilar.getScore()) {
                    mostSimilar.set(records - 1, recordSimilarity);
                    mostSimilar.sort((s1, s2) -> Float.compare(s2.getScore(), s1.getScore()));
                }
                progressBar.step();
            }
        }
        return mostSimilar;
    }

    public List<SimilarityInfo> getMostSimilarDreams(String text, int dreams, int concepts) throws Exception {
        return getMostSimilarFromDataSet(text, dreams, concepts, dreamSet);
    }

    public List<SimilarityInfo> getMostSimilarNews(String text, int articles, int concepts) throws Exception {
        return getMostSimilarFromDataSet(text, articles, concepts, newsSet);
    }

    public ToppenAfDokumentenRecord getRecord(DataSet dataSet, int recordId) {
        for (DataRecord dataRecord : dataSet.records) {
            if (dataRecord.id == recordId) {
                ToppenAfDokumentenRecord record = new ToppenAfDokumentenRecord();
                record.date = dataRecord.date.toString();
                record.text = dataRecord.comparisonText;
                return record;
            }
        }
        return null;
    }

    public ToppenAfDokumentenRecord getNews(int newsId) {
        return getRecord(newsSet, newsId);
    }

    public ToppenAfDokumentenRecord getDream(int dreamId) {
        return getRecord(dreamSet, dreamId);
    }
}
