package com.elsewhere.esa.dreams2020.comparator;

import com.elsewhere.esa.dreams2020.DayByDayComparisonRecord;

import java.util.ArrayList;
import java.util.List;

public class DayByDayComparisonSet {
    public String dreamSetName;
    public String dreamSetDate;

    public int dreamSetSize;

    public String newsSetName;
    public String newsSetDate;
    public List<DayByDayComparisonRecord> newsRecords = new ArrayList<>();

    public DayByDayComparisonSet(String dreamSetName, String dreamSetDate, int dreamSetSize, String newsSetName, String newsSetDate) {
        this.dreamSetName = dreamSetName;
        this.dreamSetDate = dreamSetDate;
        this.dreamSetSize = dreamSetSize;
        this.newsSetName = newsSetName;
        this.newsSetDate = newsSetDate;
    }

    public void addNewsRecord(DayByDayComparisonRecord record) {
        newsRecords.add(record);
    }
}
