package com.elsewhere.esa.dreams2020.comparator;

import com.dreamcloud.esa_core.similarity.TopConcept;

import java.util.List;

public class ToppenAfDokumentenComparisonSet {
    public List<TopConcept> topConcepts;
    public List<ToppenAfDokumentenRecord> news;
}
