package com.elsewhere.esa.dreams2020.server;

import com.elsewhere.esa.dreams2020.comparator.ToppenAfDokumentenComparisonSet;

public class TadResponse {
    public ToppenAfDokumentenComparisonSet results;

    public TadResponse(ToppenAfDokumentenComparisonSet results) {
        this.results = results;
    }
}
