package com.elsewhere.esa.dreams2020.server;

import com.dreamcloud.esa_core.similarity.TopConcept;

import java.util.List;

public class Dreams2020PresentationResponse {
    public float similarity;
    public String articleText;
    public String articleDate;
    public List<TopConcept> articleConcepts;
    public List<TopConcept> dreamConcepts;
    public List<TopConcept> commonConcepts;
}
