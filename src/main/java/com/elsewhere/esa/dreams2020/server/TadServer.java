package com.elsewhere.esa.dreams2020.server;

import com.dreamcloud.esa_core.similarity.SimilarityInfo;
import com.dreamcloud.esa_core.similarity.TopConcept;
import com.dreamcloud.esa_core.vectorizer.VectorBuilder;
import com.elsewhere.esa.dreams2020.comparator.ToppenAfDokumenten;
import com.elsewhere.esa.dreams2020.comparator.ToppenAfDokumentenComparisonSet;
import com.elsewhere.esa.dreams2020.comparator.ToppenAfDokumentenRecord;
import com.google.gson.Gson;
import io.javalin.Javalin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TadServer {
    private int port;
    private ToppenAfDokumenten comparator;

    private VectorBuilder vectorBuilder;

    public TadServer(int port, ToppenAfDokumenten comparator, VectorBuilder vectorBuilder) {
        this.port = port;
        this.comparator = comparator;
        this.vectorBuilder = vectorBuilder;
    }

    public TadServer(ToppenAfDokumenten comparator, VectorBuilder vectorBuilder) {
        this(8080, comparator, vectorBuilder);
    }

    public void start() throws Exception {
        System.out.println("Starting server...");
        comparator.getMostSimilarDreams("cat", 1, 0);
        comparator.getMostSimilarNews("cat", 1, 0);
        Gson gson = new Gson();

        Javalin app = Javalin.create(config -> config.enableCorsForAllOrigins()).start(port);

        app.get("/toppen", ctx -> {
            String dream = ctx.queryParam("dream");
            int records = Integer.parseInt(ctx.queryParam("records"));
            int concepts = Integer.parseInt(ctx.queryParam("concepts"));

            System.out.println("Getting most similar documents for " + dream);

            //Get the most similar news
            List<SimilarityInfo> similarNews = comparator.getMostSimilarNews(dream, records, concepts);
            List<ToppenAfDokumentenRecord> news = new ArrayList<>();

            for (SimilarityInfo info : similarNews) {
                ToppenAfDokumentenRecord record = comparator.getNews(Integer.parseInt(info.getDoc2Id()));
                if (record != null) {
                    record.similarity = info.getScore();
                    record.topConcepts = info.getTopConcepts();
                    news.add(record);
                } else {
                    throw new RuntimeException("Could not find news with id: " + info.getDoc2Id());
                }
            }

            //Get top concepts for the dream
            List<TopConcept> topConcepts = this.getTopConceptsForText(dream, concepts);

            //Construct the comparison set
            ToppenAfDokumentenComparisonSet comparisonSet = new ToppenAfDokumentenComparisonSet();
            comparisonSet.topConcepts = topConcepts;
            comparisonSet.news = news;
            ctx.status(200);
            ctx.contentType("application/json");
            ctx.result(gson.toJson(comparisonSet));
        });

        app.get("/presentation", ctx -> {
            Dreams2020PresentationResponse response = new Dreams2020PresentationResponse();
            String dream = ctx.queryParam("dream");
            int concepts = Integer.parseInt(ctx.queryParam("concepts"));

            //Get the most similar news
            List<SimilarityInfo> similarNews = comparator.getMostSimilarNews(dream, 1, concepts);
            SimilarityInfo articleSimilarity = similarNews.get(0);
            response.similarity = articleSimilarity.getScore();
            response.commonConcepts = articleSimilarity.getTopConcepts();

            ToppenAfDokumentenRecord record = comparator.getNews(Integer.parseInt(articleSimilarity.getDoc2Id()));
            response.articleText = record.text;
            response.articleDate = record.date;
            response.dreamConcepts = this.getTopConceptsForText(dream, concepts);
            response.articleConcepts = this.getTopConceptsForText(response.articleText, concepts);

            //Normalize the article and common scores
            float dreamToArticleScoreRatio = response.dreamConcepts.get(0).getScore() / response.articleConcepts.get(0).getScore();
            for (TopConcept topConcept: response.articleConcepts) {
                topConcept.setScore(topConcept.getScore() * dreamToArticleScoreRatio);
            }
            for (TopConcept topConcept: response.commonConcepts) {
                topConcept.setScore(topConcept.getScore() * dreamToArticleScoreRatio * 2);
            }

            ctx.status(200);
            ctx.contentType("application/json");
            ctx.result(gson.toJson(response));
        });

        System.out.println("Server started on port " + port);
    }

    private List<TopConcept> getTopConceptsForText(String text, int concepts) throws Exception {
        List<TopConcept> topConcepts = new ArrayList<>();
        Map<Integer, Float> documentScores = vectorBuilder.build(text).getDocumentScores();
        for (Map.Entry<Integer, Float> entry : documentScores.entrySet()) {
            TopConcept concept = new TopConcept(comparator.getNameResolver().getTitle(entry.getKey()), entry.getValue());
            topConcepts.add(concept);
        }
        topConcepts.sort((c1, c2) -> Float.compare(c2.getScore(), c1.getScore()));
        return topConcepts.subList(0, Math.min(topConcepts.size(), concepts));
    }
}
