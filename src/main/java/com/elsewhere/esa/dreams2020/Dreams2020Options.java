package com.elsewhere.esa.dreams2020;

import java.io.File;

public class Dreams2020Options {
    private String action;

    private File dreamsFile;
    private File newsFile;
    private File outputFile;

    public File getDreamsFile() {
        return dreamsFile;
    }

    public void setDreamsFile(File dreamsFile) {
        this.dreamsFile = dreamsFile;
    }

    public File getNewsFile() {
        return newsFile;
    }

    public void setNewsFile(File newsFile) {
        this.newsFile = newsFile;
    }

    public File getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(File outputFile) {
        this.outputFile = outputFile;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
