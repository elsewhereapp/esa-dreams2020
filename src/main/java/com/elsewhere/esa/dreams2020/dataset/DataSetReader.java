package com.elsewhere.esa.dreams2020.dataset;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DataSetReader {
    private final float keepRatio;
    private final int minimumWordCount;
    private final int maximumWordCount;

    public DataSetReader(float keepRatio, int minimumWordCount, int maximumWordCount) {
        this.keepRatio = keepRatio;
        this.minimumWordCount = minimumWordCount;
        this.maximumWordCount = maximumWordCount;
    }

    public DataSetReader(float keepRatio) {
        this(keepRatio, 0, 0);
    }

    public DataSetReader() {
        this(1.0f);
    }

    public DataSet read(File csvFile) throws CsvValidationException, IOException {
        return read(csvFile, csvFile.toString());
    }
    public DataSet read(File csvFile, String name) throws IOException, CsvValidationException {
        int numRead = 0;
        int totalWordCount = 0;
        int maxWordCount = 0;

        System.out.println("Reading dataset " + name + "...");
        DataSet dataSet = new DataSet(name);
        CSVReader csvReader = new CSVReader(new FileReader(csvFile));
        String[] values;
        int recordIdx = 0;
        while ((values = csvReader.readNext()) != null) {
            if (values.length < 3) {
                throw new CsvValidationException("DataSet CSV improperly formatted.");
            }

            //If the first row is a header, skip
            if (recordIdx == 0 && values[2].matches("^[a-zA-Z _-]+$")) {
                continue;
            }

            numRead++;

            //Get a random number between 0 and 1
            float random = (float) Math.random();
            if (random > keepRatio) {
                //System.out.println("Skipping record " + recordIdx + " because keep ratio is " + keepRatio + " and random number is " + random);
                recordIdx++;
                continue;
            }

            //Skip if the word count is too low or too high
            int wordCount = values[3].split(" ").length;
            totalWordCount += wordCount;

            if (wordCount > maxWordCount) {
                maxWordCount = wordCount;
            }

            if (wordCount < minimumWordCount || (maximumWordCount > 0 && wordCount > maximumWordCount)) {
                System.out.println("Skipping record " + recordIdx + " because word count is " + wordCount + ", allowed: [" + minimumWordCount + ", " + maximumWordCount + "]");
                recordIdx++;
                continue;
            }

            DataRecord record = new DataRecord();
            record.id = Integer.parseInt(values[0]);
            record.title = values[1];

            DateTimeFormatter formatter;

            //If the date is in the format YYYY-MM-DD, use that format
            if (values[2].matches("^\\d{4}-\\d{2}-\\d{2}")) {
                formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            } else if(values[2].contains("/")) {
                formatter = DateTimeFormatter.ofPattern("d/M/yyyy");
            } else {
                throw new DateTimeException("Date format not recognized: " + values[2]);
            }

            record.date = LocalDate.parse(values[2], formatter);
            record.comparisonText = this.cleanText(values[3]);
            if (record.comparisonText.equals("")) {
                System.out.println("empty doc: " + record.id + " in " + csvFile);
                recordIdx++;
                continue;
            }
            dataSet.records.add(record);
            recordIdx++;
        }

        System.out.println("Finished reading the data set.");
        System.out.println("--------------------------------");
        System.out.println("Record Reads: " + numRead);
        System.out.println("Record Kept: " + dataSet.records.size());
        System.out.println("Total word count: " + totalWordCount);
        System.out.println("Max word count: " + maxWordCount);
        System.out.println("Average word count: " + (float) totalWordCount / numRead + " (read)");
        System.out.println("Average word count: " + (float) totalWordCount / dataSet.records.size() + " (kept)");
        System.out.println("--------------------------------\n");

        return dataSet;
    }

    private String cleanText(String value) {
        //Replace non-unicode or alpha characters with spaces
        return value.replaceAll("[^\\p{L}\\d]", " ").toLowerCase();
    }
}
