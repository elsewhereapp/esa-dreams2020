package com.elsewhere.esa.dreams2020.dataset;

import java.util.Vector;

public class DataSet {
    public String name;
    public Vector<DataRecord> records = new Vector<>();

    public DataSet(String name) {
        this.name = name;
    }
}
