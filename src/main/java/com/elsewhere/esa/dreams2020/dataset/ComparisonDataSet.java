package com.elsewhere.esa.dreams2020.dataset;

import com.elsewhere.esa.dreams2020.DocumentPair;

import java.util.Vector;

public class ComparisonDataSet {
    public Vector<DocumentPair> records;

    public ComparisonDataSet(int size) {
        records = new Vector<>(size);
    }
}
