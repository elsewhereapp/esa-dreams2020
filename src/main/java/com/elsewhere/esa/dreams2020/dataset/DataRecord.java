package com.elsewhere.esa.dreams2020.dataset;

import java.time.LocalDate;

public class DataRecord {
    public int id;

    public String title;
    public LocalDate date;
    public String comparisonText;
}
