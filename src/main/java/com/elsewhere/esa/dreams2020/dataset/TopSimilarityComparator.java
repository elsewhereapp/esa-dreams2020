package com.elsewhere.esa.dreams2020.dataset;

import com.dreamcloud.esa_core.similarity.SimilarityInfo;
import com.dreamcloud.esa_core.similarity.TextSimilarity;
import com.elsewhere.esa.dreams2020.DocumentPair;

import java.util.Vector;

public class TopSimilarityComparator {
    private final TextSimilarity similarity;

    public TopSimilarityComparator(TextSimilarity similarity) {
        this.similarity = similarity;
    }

    public ComparisonDataSet compare(DataSet dataSet1, DataSet dataSet2, int topComparisons) throws Exception {
        ComparisonDataSet comparisonDataSet = new ComparisonDataSet(dataSet1.records.size() * topComparisons);

        for (DataRecord dataSet1Record: dataSet1.records) {
            Vector<DocumentPair> topSimilarities = new Vector<>(topComparisons);
            for (DataRecord dataSet2Record: dataSet2.records) {
               SimilarityInfo similarityInfo = similarity.score(dataSet1Record.comparisonText, dataSet2Record.comparisonText);
               DocumentPair similarityPair = new DocumentPair(dataSet1Record.comparisonText, dataSet2Record.title, similarityInfo.getScore());
               if (topSimilarities.size() < topComparisons) {
                   topSimilarities.add(similarityPair);
                   //Resort the vector
                   topSimilarities.sort((DocumentPair s1, DocumentPair s2) -> Float.compare(s2.getScore(), s1.getScore()));
               } else {
                   DocumentPair lastSimilarityInfo = topSimilarities.lastElement();
                   if (lastSimilarityInfo.getScore() < similarityInfo.getScore()) {
                       topSimilarities.remove(lastSimilarityInfo);
                       topSimilarities.add(similarityPair);
                       //Resort the vector
                       topSimilarities.sort((DocumentPair s1, DocumentPair s2) -> Float.compare(s2.getScore(), s1.getScore()));
                   }
               }
            }
            for (DocumentPair similarityPair: topSimilarities) {
                comparisonDataSet.records.add(similarityPair);
            }
        }

        return comparisonDataSet;
    }
}
