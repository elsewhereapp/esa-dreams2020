package com.elsewhere.esa.dreams2020.cli;

import com.dreamcloud.esa_config.cli.OptionsReader;
import com.elsewhere.esa.dreams2020.Dreams2020Options;

import java.io.File;
import java.util.Properties;

public class Dreams2020OptionsReader extends OptionsReader {
    private static final String DREAMS_FILE = "dreams2020.dreams_file";
    private static final String NEWS_FILE = "dreams2020.news_file";
    private static final String OUTPUT_FILE = "dreams2020.output_file";
    private static final String ACTION = "dreams2020.action";

    public Dreams2020Options getOptions(Properties properties) {
        Dreams2020Options options = new Dreams2020Options();
        if (hasProperty(properties, DREAMS_FILE)) {
            options.setDreamsFile(new File(properties.getProperty(DREAMS_FILE)));
        }

        if (hasProperty(properties, NEWS_FILE)) {
            options.setNewsFile(new File(properties.getProperty(NEWS_FILE)));
        }

        if (hasProperty(properties, OUTPUT_FILE)) {
            options.setOutputFile(new File(properties.getProperty(OUTPUT_FILE)));
        }

        if (hasProperty(properties, ACTION)) {
            options.setAction(properties.getProperty(ACTION));
        }
        return options;
    }
}
