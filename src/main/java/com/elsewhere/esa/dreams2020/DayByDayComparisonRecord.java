package com.elsewhere.esa.dreams2020;

import com.dreamcloud.esa_core.similarity.SimilarityInfo;
import com.dreamcloud.esa_core.similarity.TopConcept;

import java.util.List;

public class DayByDayComparisonRecord {
    public String date;
    public int recordSize;
    public float similarity;
    public int wordCount;
    public List<TopConcept> topConcepts;
    public int highScores = 0;
    public int indicativeScores = 0;
    public int lowScores = 0;
    public int indiscernibleScores = 0;
    public List<SimilarityInfo> examples;

    public DayByDayComparisonRecord(String date, int recordSize, float similarity, int wordCount, List<TopConcept> topConcepts, List<SimilarityInfo> examples) {
        this.date = date;
        this.recordSize = recordSize;
        this.similarity = similarity;
        this.wordCount = wordCount;
        this.topConcepts = topConcepts;
        this.examples = examples;
    }
}
