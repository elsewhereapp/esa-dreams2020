package com.elsewhere.esa.dreams2020;

import com.dreamcloud.esa_config.cli.AnalyzerOptionsReader;
import com.dreamcloud.esa_config.cli.CollectionOptionsReader;
import com.dreamcloud.esa_config.cli.TfIdfOptionsReader;
import com.dreamcloud.esa_config.cli.VectorizationOptionsReader;
import com.dreamcloud.esa_core.analyzer.AnalyzerOptions;
import com.dreamcloud.esa_core.analyzer.EsaAnalyzer;
import com.dreamcloud.esa_core.analyzer.TokenizerFactory;
import com.dreamcloud.esa_core.similarity.DocumentSimilarity;
import com.dreamcloud.esa_core.similarity.SimilarityInfo;
import com.dreamcloud.esa_core.similarity.TextSimilarity;
import com.dreamcloud.esa_core.similarity.TopConcept;
import com.dreamcloud.esa_core.vectorizer.*;
import com.dreamcloud.esa_score.analysis.TfIdfAnalyzer;
import com.dreamcloud.esa_score.analysis.TfIdfOptions;
import com.dreamcloud.esa_score.analysis.TfIdfStrategyFactory;
import com.dreamcloud.esa_score.analysis.strategy.TfIdfStrategy;
import com.dreamcloud.esa_score.fs.*;
import com.dreamcloud.esa_score.score.DocumentNameResolver;
import com.dreamcloud.esa_core.vectorizer.scoreMod.PruneScoreMod;
import com.dreamcloud.esa_core.vectorizer.scoreMod.VectorLimitScoreMod;
import com.elsewhere.esa.dreams2020.cli.Dreams2020OptionsReader;

import com.elsewhere.esa.dreams2020.comparator.DayByDayComparator;
import com.elsewhere.esa.dreams2020.comparator.RoundRobinComparator;
import com.elsewhere.esa.dreams2020.comparator.ToppenAfDokumenten;
import com.elsewhere.esa.dreams2020.dataset.*;
import com.elsewhere.esa.dreams2020.server.TadServer;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizer;

import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        //Set up the ESA properties
        String propertiesPath =  "./esa.properties";
        Properties properties = new Properties();
        properties.load(new FileInputStream(propertiesPath));

        System.out.println("Properties");
        System.out.println("----------------------------------------");
        properties.stringPropertyNames().stream().sorted().forEach((String property) -> {
            System.out.println(property + "\t\t=  " + properties.getProperty(property));
        });
        System.out.println("----------------------------------------\n");

        VectorizationOptions vectorOptions = new VectorizationOptionsReader().getOptions(properties);
        AnalyzerOptions analyzerOptions = new AnalyzerOptionsReader().getOptions(properties);
        TfIdfOptions tfIdfOptions = new TfIdfOptionsReader().getOptions(properties);
        CollectionOptions collectionOptions = new CollectionOptionsReader().getOptions(properties);
        Dreams2020Options dreams2020Options = new Dreams2020OptionsReader().getOptions(properties);

        analyzerOptions.setTokenizerFactory(new TokenizerFactory() {
            public Tokenizer getTokenizer() {
                return new StandardTokenizer();
            }
        });

        try {
            TfIdfStrategyFactory tfIdfFactory = new TfIdfStrategyFactory();
            TfIdfStrategy tfIdfStrategy = tfIdfFactory.getStrategy(tfIdfOptions);

            TfIdfAnalyzer tfIdfAnalyzer = new TfIdfAnalyzer(tfIdfStrategy, new EsaAnalyzer(analyzerOptions), collectionOptions.getTermIndex());
            VectorBuilder vectorBuilder = new VectorBuilder(collectionOptions.getScoreReader(), tfIdfAnalyzer, analyzerOptions.getPreprocessor(), vectorOptions);

            PruneScoreMod pruneScoreMod = new PruneScoreMod(vectorOptions.getWindowSize(), vectorOptions.getWindowDrop());
            VectorLimitScoreMod vectorLimitScoreMod = new VectorLimitScoreMod(vectorOptions.getVectorLimit());
            vectorBuilder.addScoreMod(pruneScoreMod);
            vectorBuilder.addScoreMod(vectorLimitScoreMod);

            DocumentNameResolver nameResolver =  collectionOptions.getDocumentNameResolver();
            TextVectorizer textVectorizer = new Vectorizer(vectorBuilder);

            if (dreams2020Options.getDreamsFile() == null || !dreams2020Options.getDreamsFile().exists()) {
                throw new FileNotFoundException("The Dreams 2020 dream dataset file could not be found.");
            }
            if (dreams2020Options.getNewsFile() == null || !dreams2020Options.getNewsFile().exists()) {
                throw new FileNotFoundException("The Dreams 2020 news dataset file could not be found.");
            }

            DataSetReader dreamDataSetReader = new DataSetReader(1.0f, 1, 0);
            DataSet dreamSet = dreamDataSetReader.read(dreams2020Options.getDreamsFile());

            DataSetReader newsDataSetReader = new DataSetReader(1.0f, 10, 200);
            DataSet newsSet = newsDataSetReader.read(dreams2020Options.getNewsFile());
            TextSimilarity similarity = new DocumentSimilarity(textVectorizer);

           switch (dreams2020Options.getAction()) {
               case "stats":
                   System.out.println("Dream-News Stats:");
                   System.out.println("Dreams: " + dreamSet.records.size());
                   System.out.println("News: " + newsSet.records.size());
                   System.out.println("Comparison Count: " + dreamSet.records.size() * newsSet.records.size());
                   break;
               case "compare":
                   if (dreams2020Options.getOutputFile() == null || !dreams2020Options.getOutputFile().exists() || !dreams2020Options.getOutputFile().isDirectory()) {
                       throw new FileNotFoundException("The Dreams 2020 news dataset file could not be found.");
                   }

                   System.out.println("Dream-News Stats:");
                   System.out.println("Dreams: " + dreamSet.records.size());
                   System.out.println("News: " + newsSet.records.size());
                   System.out.println("Comparison Count: " + dreamSet.records.size() * newsSet.records.size());
                   System.out.println("\nBeginning a round robin comparison...\n");

                   RoundRobinComparator comparator = new RoundRobinComparator(similarity);
                   comparator.compare(dreamSet, newsSet, dreams2020Options.getOutputFile());
                   break;
               case "top":
                   System.out.println("Dream-News Stats:");
                   System.out.println("Dreams: " + dreamSet.records.size());
                   System.out.println("News: " + newsSet.records.size());
                   System.out.println("Comparison Count: " + dreamSet.records.size() * newsSet.records.size());
                   System.out.println("\nBeginning top comparisons...\n");

                   TopSimilarityComparator topComparator = new TopSimilarityComparator(similarity);
                   ComparisonDataSet topComparisonSet = topComparator.compare(dreamSet, newsSet, 3);

                   Map<String, Vector<DocumentPair>> pairMap = new HashMap<>();
                   for (DocumentPair pair : topComparisonSet.records) {
                       Vector<DocumentPair> pairVector = pairMap.getOrDefault(pair.getDoc1(), new Vector<>());
                        pairVector.add(pair);
                        pairMap.put(pair.getDoc1(), pairVector);
                   }

                   for (String doc: pairMap.keySet()) {
                       System.out.println(doc);
                       System.out.println("----------------------------------------");
                       for (DocumentPair pair : pairMap.get(doc)) {
                           System.out.println(pair.getDoc2() + ": " + pair.getScore());
                       }
                       System.out.println("----------------------------------------");
                   }
                   break;

               case "day-by-day":
                   DayByDayComparator dayByDayComparator = new DayByDayComparator(similarity, dreamSet, newsSet, dreams2020Options.getOutputFile(),nameResolver);
                   dayByDayComparator.compare(2020, 1, 1, 12, 31, 30 * 6, 10);
                   break;
               case "top-news-article":
                    ToppenAfDokumenten topNews = new ToppenAfDokumenten(similarity, dreamSet, newsSet, nameResolver);
                    List<SimilarityInfo> similarNews = topNews.getMostSimilarNews(properties.getProperty("dreams2020.text"), 3, 10);
                    System.out.println("Top news article: " + similarNews.get(0).getScore());
                    System.out.println("----------------------------------------");
                    System.out.println(similarNews.get(0).getDoc2Id());
                    System.out.println("----------------------------------------");
                   break;
               case "top-dream":
                   ToppenAfDokumenten topDreams = new ToppenAfDokumenten(similarity, dreamSet, newsSet, nameResolver);
                   Scanner scanner = new Scanner(System.in);

                   String dream = "";
                     while(!dream.equals(":q")) {
                          System.out.println("Enter dream: ");
                          dream = scanner.nextLine();
                          List<SimilarityInfo> similarDreams = topDreams.getMostSimilarDreams(dream, 3, 10);
                          System.out.println("Top dream: " + similarDreams.get(0).getScore());
                          System.out.println("----------------------------------------");
                          System.out.println(similarDreams.get(0).getDoc2Id());
                          System.out.println("Shared Concepts: ");
                          System.out.println("----------");
                            for (TopConcept concept : similarDreams.get(0).getTopConcepts()) {
                                System.out.println(concept.getConcept() + ": " + concept.getScore());
                            }
                            System.out.println("----------");
                          System.out.println("----------------------------------------");
                     }
                   break;
               case "toppen-server":
                    ToppenAfDokumenten toppen = new ToppenAfDokumenten(similarity, dreamSet, newsSet, nameResolver);
                    TadServer server = new TadServer(8080, toppen, vectorBuilder);
                    server.start();
                    break;
               default:
                   System.out.println("Unrecognized action: " + dreams2020Options.getAction());
           }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
