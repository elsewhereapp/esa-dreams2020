# Dreams2020 Research Project
This is the main repository for the Dreams2020 research project. Here you will find all of the instructions for reproducing our work, and links to the code repositories and data sets which we used.

This guide will walk you through the steps to reproduce our work. This includes:
- Setting up the environment
- Installing the repositories
- Downloading the data sets
- Building an ESA index
- Verifying the ESA index using Spearman and Pearson correlations
- Generating dream-news similarity profiles

## 1. Setting up the environment

### 1) Install Java

If you haven't set up Java on your computer, you'll need that.

**On Mac**

`brew install java`


Also, you'll need the Java Development Kit:
https://www.oracle.com/java/technologies/downloads/#jdk17-mac]
Use the `Arm 64 DMG Installer` or `x64 DMG Installer`, depending on your system.

**On Windows**

Download and install the latest version of Java from:
https://www.oracle.com/java/technologies/downloads/#jdk17-windows

### 2) Install Maven
**On Mac**

`brew install maven`

**On Windows**

Download a Maven binary zip archive from:
https://maven.apache.org/download.cgi

Extract it to a location of your choice. Drill down into the directory structure until you find a "bin" folder. Add this to the system path via an environment variable.

To verify your installation run `mvn -v` from a new terminal window and ensure the command is found.

If there are any issues, check out the Maven installation website:
https://maven.apache.org/install.html
and this SO page which helped us to resolve some issues:
https://stackoverflow.com/questions/26609922/maven-home-mvn-home-or-m2-home

## 2. Installing the repositories
We have divided our various tools and packages into a handful of code repositories which will need to be downloaded and installed through Maven. Each of these is described in detail in the appendix.

You will always need to install the three dependencies below, and then build the individual packages that use them for the specific tasks you want to perform.

### 1) Install ESA dependencies
To use any of the standalone ESA packages, you will need to download the `esa-score`, `esa-core`, and `esa-config` libraries and install them with Maven.
- Download the relevant repositories located at the following links:
    - https://gitlab.com/elsewhereapp/esa-score
    - https://gitlab.com/elsewhereapp/esa-core
    - https://gitlab.com/elsewhereapp/esa-config
- Enter the directories of each repository
- Run `mvn install` to install the package

### 2) Install esa-wiki package
Download the `esa-wiki` repository located at https://gitlab.com/elsewhereapp/esa-wiki.

Enter the esa-wiki directory and run `mvn pakcage`.

This compiles the Java code, links to all relevant dependencies, and generates a stand-alone executable JAR file in the "target" folder.

This will be used later for working with Wikipedia dumps and building an ESA index.

### 3) Install esa-tuner package
Download the `esa-tuner` repository located at https://gitlab.com/elsewhereapp/esa-tuner.

Enter the esa-tuner directory and run `mvn pakcage`.

This compiles the Java code, links to all relevant dependencies, and generates a stand-alone executable JAR file in the "target" folder.

This will be used later for tuning and verifying our ESA index.

### 4) Install esa-dreams2020 package
Download the `esa-dreams2020` repository located at https://gitlab.com/elsewhereapp/esa-dreams2020.

Enter the esa-dreams2020 directory and run `mvn pakcage`.

This compiles the Java code, links to all relevant dependencies, and generates a stand-alone executable JAR file in the "target" folder.

This will be used for generating dream-news similarity profiles.

### 4) Install esa-visualizer package
Download the `esa-visualizer` repository located at https://gitlab.com/elsewhereapp/esa-visualizer.

Enter the esa-visualizer directory and run `mvn pakcage`.

This compiles the Java code, links to all relevant dependencies, and generates a stand-alone executable JAR file in the "target" folder.

This will be used for visualizing the dream-news similarity profiles.

## 3. Downloading the data sets
We used many datasets for the different parts of this project, and here you will find them all listed and linked. These will be referenced later on in the guide.

### a) Wikipedia dumps
We used a Wikipedia dump from 2021-12-01 for our ESA index. We have shared this on Google Drive and you can download it [here](https://drive.google.com/drive/folders/1hBx6CSqpNTCj8I5IBiSTy3F5uy-TAHFF?usp=drive_link).

If you want to use a newer Wikipedia dump, a current dump is always available from the Wikimedia Foundation here: https://dumps.wikimedia.org/enwiki/

A list of all available database dumps is available *here*: <https://dumps.wikimedia.org/backup-index-bydb.html>.

And a selection of historical database dumps are here: https://dumps.wikimedia.org/archive/

For any of these dumps, you want to download the file named as "enwiki-YYYYMMDD-pages-articles-multistream.xml.bz2 ".
![image](https://user-images.githubusercontent.com/14936307/145387013-26238b20-8be5-4803-9775-281231ac1c45.png)

For older dumps, the "multistream" part might be excluded and may require a small configuration change to XML parsing.

If you are looking for a smaller database dump that will be easier to work with, we recommend using Simple Wikipedia.
![image](https://user-images.githubusercontent.com/14936307/145384562-2431a7d5-bd36-454c-8779-241414e1f5a9.png)


Save the file to a location of your choice, which will refer to as $WIKI_PATH in the rest of the guide and in example configuration files.

### b) Dream Data Set
We used a set of dreams compiled for the Dreams2020 project, and which can be accessed within the Sleep and Dream Database (https://sleepanddreamdatabase.org).

The set includes 2,201 dreams from the year of 2020 and was compiled from the dream journals of 10 individuals.

You can download this as a CSV and use it, or use a preprocessed and formatted CSV file which you can find in our Google Drive here: [google drive link].

### c) News Data Set
We used a news dataset from the Associated Press, which included roughly 100 top-wired news articles from each day of the year of 2020. We used the summaries from a selection of these. 

Unfortunately, we do not have permissions to release this dataset, and we suggest that the reader who wishes to reproduce this kind of work use a similar dataset. One possibility is "reddit news", an openly available dataset of news articles from reddit's r/news subreddit.

### d) WordSim-353 Data Set
The WordSim-353 data set is a set of 353 word pairs with human-annotated similarity scores. It is used to evaluate the quality of ESA implementations for word relatedness.

We used the standard WordSim-353 dataset for verifying our ESA index. You can download this from our Google Drive here: [google drive link]. 

Note that this dataset only includes 351 word pairs. The original contained 353, but one pair was a duplicate, and another pair uses a 2-letter word which are excluded from our ESA index. 

### e) LP-50 Data Set
The LP-50 data set is a set of 1,225 document pairs with human-annotated similarity scores. These document pairs are composed of 50 news summaries compared to one another in a round-robin fashion.

It is used to evaluate the quality of ESA implementations for document relatedness.

### f) English Stopwords
We used an extended list of English stopwords, which can be found in our Google Drive here: [google drive link].

We found that excluding more potentially unimportant words from our ESA index improved the accuracy of the tool considerably. 

### g) Dictionary Words
We experimented with a large dictionary of English words and only allowing those words in our final ESA index. This did not significantly affect the accuracy of the tool, but it did reduce the size of the index considerably.

We did not use this in our final implementation, but you can find the dictionary in our Google Drive here: [google drive link].

## 4. Building the ESA index
Let's go over the steps required to build a working ESA index. We'll use Simple Wikipedia as an example, but you can use any other Wikipedia dump you like.

### 1) Install the `esa-wiki` package
Install the ESA dependencies and the `esa-wiki` package as described above.

### 2) Download the Wikipedia dump
Download the Wikipedia dump you want to use per the instructions above.

### 3) Update the configuration file
There is a configuration template file located within the `esa-wiki` repository. It's called `esa.properties.template` and you will need to copy it to `esa.properties`.

You will need to update the following settings:
**$WIKI_PATH** - the path to folder where you saved the Wikipedia dump
**$WIKI_SOURCE** - the name of the Wikipedia dump file (without the path)
**$OUTPUT_DIR** - the path to the folder where you want to save the ESA index (and other files which are generated along the way)

Depending on the Wikipedia dump used, you will want to adjust the term and link counts. For reference, 2005-6 Wikipedia should use a minimum of 100 terms and 5 incoming and outgoing links.

For 2011 Wikipedia, you can increase these to 200, 14, and 14. 

For 2021 Wikipedia, we used 400, 20, and 20.

```properties
# Minimum terms in article
article.minTerms = 400

# Minimum incoming links
article.minInLinks = 20

# Minimum outgoing links
article.minOutLinks = 20
```

You may also want to update the settings for threading, depending on your system.

```properties
indexing.threads.count = 20
indexing.threads.batchSize = 1000
```

### 4) Run ESA indexing stages
You will see that there are 6 different indexing stages mentioned in the configuration file.

```properties
indexing.stage.0 = preprocess
#indexing.stage.1 = id-titles
#indexing.stage.2 = count
#indexing.stage.3 = rare-terms
#indexing.stage.4 = index
#indexing.stage.5 = stats
```

For a small Wikipedia dump such as Simple Wikipedia, you can uncomment all 6 stages and run them all at once. For larger Wikipedia dumps, you will want to run them one at a time. The sixth step is optional and is only used to generate some statistics about the index.

To run the indexing stages, you will need to run the `esa-wiki` jar file. You can do this by running one of the provided scripts:

_Linux/Mac_
```bash
./esa.sh
```

_Windows_
```bash
esa.bat
```

You may need to edit the .sh or .bat file to allow the JVM to use more memory, depending on the size of the Wikipedia dump you are using.

If running on Linux/Mac, you may need to make the script executable first:

```bash
chmod +x esa.sh
```


### 5) Inspecting the ESA index
Once all stages have been run, you will have a complete ESA index. This index can be referenced and used by both the `esa-tuner` and `esa-dreams2020` tools described in the next sections.

The files that make up the ESA index will be written to the **$OUTPUT_DIR** folder, and are as follows:
* `term-index.esa` (binary) - The terms in the ESA index, statistics about their usage, and a file pointer into the scores file for where to find the document scores for each term.
* `document-scores.esa` (binary) - The document scores for all terms within the index.
* `rare-terms.txt` - A list of rare terms which were excluded from the index.
* `id-titles.txt` (binary) - A file containing the document IDs and titles for each document in the index.

A handful of other intermediate files are also generated along the way, but they can be deleted once the index is built. They contain various forms of the Wikipedia articles with annotated metadata, redirect maps, and other information.

## 5. Verifying and tuning the ESA index
Once you have a complete index, it is time to verify that it is accurate. We have provided a tool called `esa-tuner` which can be used to find the best settings for your index and to verify its accuracy.

There are a handful of options that you can apply to an index that will change its accuracy and performance, and finding the ideal settings can be a challenge.

### 1) Build or download an ESA index
You can either build an ESA index using the instructions above, or you can download one of the pre-built indexes from our Google Drive here: [google drive link].

### 2) Install the `esa-tuner` package
Install the ESA dependencies and the `esa-tuner` package as described above.

### 3) Update the configuration file
There is a configuration template file located within the `esa-tuner` repository. It's called `esa.properties.template` and you will need to copy it to `esa.properties`.

You will need to update the following settings to point to the files for the ESA index that was previously built or downloaded:

```properties
# ESA index
collection.termIndexSource = $OUTPUT_DIR/term-index.esa
collection.documentScoreSource = $OUTPUT_DIR/document-scores.esa

# Optional map for getting document titles from IDs
collection.titleSource = $OUTPUT_DIR/id-titles.txt

# Stop words and rare words
analyzer.stop.0.source = en-stopwords.txt
analyzer.stop.1.source = $OUTPUT_DIR/rare-terms.txt
```

### 4) Verify the index
ESA tools are validated by testing their word and document similarity using the WordSim-353 and LP50 data sets where the tools results are compared to human assessments. 

To get this to work properly, you will need to specify your tuning settings and set the action of the tool to `verify`.

There are a variety of settings which you can tune, and understanding these will be necessary for any application of ESA. The following are the settings that you can adjust for working with different indexes and data sets:
- Vectorization options
  
  ESA computes the similarity of texts by converting them into vectors of document relatedness scores and applying cosine similarity. The complexities of this will be ignored, but some settings for creating these vectors will be useful to know for tuning.
  ```properties
  # The size of the sliding window
  vector.windowSize = 7

  # The maximum allowed percentage drop from the first item in the window to the last
  vector.windowDrop = 0.000002

  # The maximum number of entries in a vector
  vector.limit = 6000

  # The concept multiplier
  vector.conceptMultiplier = 1.28
  ```
  The `vector.windowSize` and `vector.windowDrop` settings are applied to prune irrelevant documents when finding related documents for each term in the input text.
  
  The inner workings of this is beyond the scope of this guide, but the gist of this is that you can limit more potentially irrelevant documents from being considered by either increasing the window size or lowering the window drop.

  It should be noted that these values should be adjusted quite radically as many configurations can produce good results. For example, with the original 2006 ESA implementation, values of **100** and **0.05** were used, whereas with 2021 Wikipedia we used **7** and **0.000002**

  The `vector.limit` setting limits the total number of document scores for each input text. You can think of this setting as causing ESA to look for more and more tenuous connections to compute document similarity. There is no right number, but we have found values between 500 and 10000 to be reasonable depending on the Wikipedia dump used.

  Generally, the smaller the ESA index, the lower this value can be.

  The `vector.conceptMultiplier` specifies what to do when multiple terms from an input text are correlated with the same document. By default, ESA simply adds the two values, but we found that applying an additional multiplier improved accuracy consistently. You can experiment with changes to this value, but we have found that values between 1.0 and 1.3 work well.
- Scoring algorithm options

  The document similarity scores mentioned above are calculated using TF-IDF and related algorithms. These settings can be adjusted to change the weighting of terms and documents.
  ```properties
  # The three letter code can also be prefixed with "b:" to make it BM25
  scoring.mode = ltc
  
  # The "b" parameter of BM25 (traditionally 0.75)
  # This can be 1 for BM11 or 0 for BM15
  scoring.bm25.b = 0.75
  
  # The "k" parameter of BM25 (traditionally 1.2 or 2.0)
  scoring.bm25.k = 1.2
  
  # The optional delta which turns BM25 into BM25+ (traditionally set to 1.0)
  scoring.bm25.delta = 1.0
  ```
  The `scoring.mode` setting is the most important part and determines the kind of algorithm to be used. Each letter of the three-letter code determines a different part of the algorithm. The first specifies the type of Term-Frequency to use. The second specifies Inverse Document Frequency, and the third specifies the normalization method.
  
  The meanings of these values can be found here:

  https://nlp.stanford.edu/IR-book/html/htmledition/document-and-query-weighting-schemes-1.html

  We added a few additional options which you can find in the code, the notable one being the IDF method 'b' which is specifically for BM25.

  **BM25**

  By default, the 3 letter code denotes a form of TF-IDF. To instead use the newer and more accurate BM25 algorithm, you can prefix the code with `b:`. This will use the BM25 algorithm with the default parameters, but you can also adjust the `b` and `k` parameters as well as the optional `delta` parameter.

  To use **BM25+**, you can specify the delta parameter to a value of greater than 0.

  To use **BM11**, you can set the `b` parameter to 1.

  To use **BM15**, you can set the `b` parameter to 0.

  For each of these you can also simply specify the name of the algorithm as `scoring.mode` = `bm25`, `bm25+`, `bm11`, and `bm15`.

Once you have applied your preferred settings, you will need to set the action of the tool to `verify`.
```properties
tuner.action = verify
```

Next you can run the tool using the following command:
_Linux/Mac_
```bash
./esa.sh
```

_Windows_
```bash
esa.bat
```

You may need to edit the .sh or .bat file to allow the JVM to use more memory, depending on the size of the Wikipedia dump you are using.

If running on Linux/Mac, you may need to make the script executable first:

```bash
chmod +x esa.sh
```

This will run a Spearman correlation on the WordSim-353 data set and a Pearson correlation on the LP50 data set. The p-values for each of these will be logged to the console.

### 5) Tuning the index
Because it takes time to very an ESA index and there are many possible variables to tune--and variables where very small changes can drastically impact the results--we have provided a tool which can automate some of these tunings. 

The tool run a series of verifications while incrementing vectorization values, and will report back with the most accurately tuned configuration.

There are two types of tunings that you can do:
1. Vector limit tuning
2. Sliding window tuning

In (1) you can specify low, high, and step value for the vector limit and the tool will run through each step and log the best value.
```properties
# The vector limit start
tuning.tune.vectorLimitStart = 6000

# The vector limit end
tuning.tune.vectorLimitEnd = 6000

# The vector limit step
tuning.tune.vectorLimitStep = 1
```

In (2) you specify the low, high, and step values for both the window size and window drop options. All combinations are iterated through and the best configuration is logged. 
```properties
## The "window" is a sliding window that moves across document scores for a term
## The scores are sorted from highest to lowest.


# The initial size of the window
tuning.tune.windowStart = 7

# The ending size of the window
tuning.tune.windowEnd = 8

# The window step
tuning.tune.windowStep = 1

## Window drop is the percentage to use when determining if the first score in the window
## and the last score in the window are too similar to keep going
## This roughly translates to being in the "tail" of the scores
## At this point we truncate the window and only keep the scores seen so far

# The starting drop off
tuning.tune.dropOffStart = 0.000001

# The ending drop off
tuning.tune.dropOffEnd = 0.000003

# The drop off step
tuning.tune.dropOffStep = 0.000001
```

These two types of tunings can be applied to either tuning word relatedness or document relatedness (but not both at the same time) by setting the tuning type to either `spearman` or `pearson`.
```properties
tuning.tune.type = pearson
```

To run the tuning, you will need to set the action of the tool to `tune` and the strategy to `esa`. We experimented briefly with supplementing ESA with Word2Vec, but we only support ESA at this time.
```properties
tuning.action = tune
tuning.tune.strategy = esa
```

Next you can run the tool using the following command:
_Linux/Mac_
```bash
./esa.sh
```

_Windows_
```bash
esa.bat
```

You may need to edit the .sh or .bat file to allow the JVM to use more memory, depending on the size of the Wikipedia dump you are using.

If running on Linux/Mac, you may need to make the script executable first:

```bash
chmod +x esa.sh
```

## 6. Generating dream-news similarity profiles
One of the main goals of the Dreams2020 research project was to see what effect the news events in the year of 2020 had on dreams.

One theory, the "continuity hypothesis" states that dreams tend to most often be about ordinary life events and that what happens in our day-to-day lives is reflected in our dreams. If this is true, then we would expect to see a correlation between what is happening in the news and what is happening in people's dreams, with a strong connection between how close the two are in time. This is especially true during a year such as 2020 when people's lives were significantly impacted by events which were heavily broadcast on the news in a real-time manner. 

To test this hypothesis, we used our ESA tool to compare the similarity of dream reports to news articles to see how the similarity tracked across time and distance between dreams and news.

Here we will illustrate how you can use this part of the tool to perform a similar analysis by performing a large number of comparisons between documents at specific dates to determine how time effects the similarity.


### 1) Build or download an ESA index
You can either build an ESA index using the instructions above, or you can download one of the pre-built indexes from our Google Drive here: [google drive link].

### 2) Install the `esa-dreams2020` package
Install the ESA dependencies and the `esa-dreams2020` package as described above.

### 2) Acquire two document data sets
You can download our dream document data set mentioned above, but will need to provide a second set of documents to compare the dreams to. You can use any data set you like as long as it is in the correct format.

The format for the data sets is a CSV file with the following columns:
* `id` - a unique identifier for the document
* `title` - the title of the document (optional, can be blank)
* `date` - the date of the document in the format `YYYY-MM-DD`
* `text` - the text of the document

The file may have an optional header row which will be ignored by the tool.

### 4) Update the configuration file
There is a configuration template file located within the `esa-dreams2020` repository. It's called `esa.properties.template` and you will need to copy it to `esa.properties`.

You will need to update the settings mentioned in the `esa-tuner` section above. These are the ESA index, vectorization, and scoring settings. 

You will further need to set the options for the two data sets which you acquired above.

```properties
dreams2020.news_file = ./all-news-final-2020.csv
dreams2020.dreams_file = ./all-dreams-final-2020.csv
```

### 5) Generating the similarity profiles
Once all of the settings are configured, you can run the tool using the following command:

_Linux/Mac_
```bash
./esa.sh
```

_Windows_
```bash
esa.bat
```

You may need to edit the .sh or .bat file to allow the JVM to use more memory, depending on the size of the Wikipedia dump you are using.

If running on Linux/Mac, you may need to make the script executable first:

```bash
chmod +x esa.sh
```

This will generate a CSV file with the following columns (no headers):
* `days_between` - the number of days between the news and the dream (negative if the news is before the dream)
* `similarity` - the similarity score between the news and the dream (a floating point number between 0 and 1)

This file can be analyzed to generate a graph of similarity scores across time by running it as input into the `esa-visualizer` tool. This tool works the same as the rest in that it has `esa.sh` and `esa.bat` scripts and is configured with a simple `esa.properties` file. You can download this from our Gitlab repository here:

https://gitlab.com/elsewhereapp/esa-visualizer

The output of the visualizer tool will be a simpler (and much smaller) CSV file from which a chart can be manually generated to show how the time between the documents affects their similarity.

An example of the output of the visualizer tool is shown below:

![image](https://gitlab.com/elsewhereapp/esa-dreams2020/-/wikis/uploads/45aa0e30cf0de1d5b6f753a345f4de63/image.png)

## License

This software is provided under the terms of the MIT license, which pretty much lets you do anything you want with the software provided you leave our copyright notice and don't hold us responsible for any liability.

## Contact
You can contact the authors of this software here on Gitlab or by sending an email to sheldonjuncker@gmail.com


## References

## Appendix

### 1) Repositories
- `esa-wiki`: A command line tool and library for processing Wikipedia XML dumps and generating a final preprocessed corpus taking into account: templates, variables, term counts, link counts, English stopwords and dictionary words, rare words, and more.
- `esa-core`: The core ESA implementation including text analysis, document preprocessing, and vectorization.
- `esa-score`: The ESA scoring library which contains a set of scoring algorithms (various versions of TF-IDF and BM25) and file system writing and reading tools for working with written score indexes. .
- `esa-server`: A simple HTTP server for vectorizing documents and comparing text similarity.
- `esa-tuner`: A tool for verifying the accuracy of an ESA implementation using the WordSim-353 dataset with Spearman and the LP-50 document dataset with Pearson. It also includes a tool for fine-tuning an ESA implementation.
- `esa-dreams2020`: A tool for generating similarity profiles between news articles and dream reports (or any document sets)
- `esa-visualizer`: A tool for visualizing similarity profiles generated by `esa-dreams2020`
- `esa-config`: A library for managing configuration files and command line arguments for all of the other ESA libraries

Each of these libraries includes a few CLI tools for creating the various configuration classes from CLI options. This is useful if you want to create a CLI tool for interfacing with the library.
