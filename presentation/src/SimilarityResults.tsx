import Vector from "./vector/Vector";
import { VectorChart } from "./vector/VectorChart";

export default function SimilarityResults({ results }: { results: any }) {
  const normalizeVectors = (vectors: Vector[], normalizedMax: number) => {
    let max = 0;
    for (const vector of vectors) {
      if (vector.getMagnitude() > max) {
        max = vector.getMagnitude();
      }
    }

    const multiplier = normalizedMax / max;
    vectors.forEach(dv => {
      dv.setMagnitude(dv.getMagnitude() * multiplier);
    });
  };

  const buildVectors = (concepts: any[], commonConcepts: any[], otherConcepts: any[]): Vector[] => {
    const normalizeLabel = (label: string): string => {
      return label.charAt(0).toUpperCase() + label.substring(1);
    };

    const vectors: Vector[] = [];

    const positions = [200, 45, 80, 150, 0, 250];

    let conceptsFound = 0;
    //Find up to 3 concepts that are not common
    for (const concept of concepts) {
      const label = concept.concept;
      const score = concept.score;

      if (commonConcepts.find((cc: any) => cc.concept == label)) {
        continue;
      }

      vectors.push(new Vector(normalizeLabel(label), positions[conceptsFound], score, false));

      if (++conceptsFound == 3) {
        break;
      }
    }

    //Add up to 3 common concepts
    let commonConceptsFound = 0;
    for (const commonConcept of commonConcepts) {
      const concept = concepts.find((c: any) => c.concept == commonConcept.concept);
      const otherConcept = otherConcepts.find((c: any) => c.concept == commonConcept.concept);
      if (!concept || !otherConcept) {
        continue;
      }

      vectors.push(
        new Vector(normalizeLabel(concept.concept), positions[commonConceptsFound + 3], concept.score, true)
      );

      if (++commonConceptsFound == 3) {
        break;
      }
    }

    //Normalize all dream vectors so that the longest is 400
    normalizeVectors(vectors, 250);

    return vectors;
  };

  //Build the dream vectors
  const dreamVectors = buildVectors(results.dreamConcepts, results.commonConcepts, results.articleConcepts);
  console.log("dreamVectors", dreamVectors);

  const articleVectors = buildVectors(results.articleConcepts, results.commonConcepts, results.dreamConcepts);

  const cosineVectors: Vector[] = [];
  for (const dv of dreamVectors) {
    if (dv.isCommon()) {
      const av = articleVectors.find(av => av.getLabel() == dv.getLabel());
      if (av) {
        cosineVectors.push(new Vector(dv.getLabel(), dv.getDirection(), dv.getMagnitude() + av.getMagnitude(), true));
      }
    }
  }

  normalizeVectors(cosineVectors, 250);

  return (
    <div className="results-area">
      <h2 style={{ textAlign: "center" }}>Vectorization</h2>
      <div className="vectorization-vectors">
        <div className="vector-wrapper">
          <VectorChart key={"dreams"} vectors={dreamVectors} />
        </div>
        <div className="vector-wrapper">
          <VectorChart key={"articles"} vectors={articleVectors} />
        </div>
      </div>
      <h2 style={{ textAlign: "center" }}>Cosine Similarity</h2>
      <div className="vector-wrapper">
        <VectorChart key={"cosine"} vectors={cosineVectors} />
      </div>
      <h3 style={{ fontSize: 30 }}>Similarity = {results.similarity}</h3>
    </div>
  );
}
