import Vector from "./Vector";
import React from "react";
import {VectorArrow} from "./VectorArrow";

export const VectorChart = ({ vectors }: { vectors: Vector[]}) => {
    return (
        <div style={{
            position: "relative",
            width: 400,
            height: 400,
            margin: '0 auto',
            marginTop: -50
        }}>
            {vectors.map((v, i) => (
                <VectorArrow vector={v} key={i} />
            ))}
        </div>
    );
}