import Vector from "./Vector";
import React from "react";

export const VectorArrow = ({ vector }: { vector: Vector }) => {
  return (
    <div
      style={{
        position: "absolute",
        width: vector.getMagnitude() + "px",
        transform: "rotate(" + vector.getDirection() + "deg)",
        transformOrigin: "left bottom",
        left: 200,
        top: 200,
      }}
      className="vector-arrow"
    >
      <span
        style={{
          position: "absolute",
          transform: "rotate(-" + vector.getDirection() + "deg)",
          transformOrigin: "center",
          marginTop: -15,
          // paddingLeft: 15
        }}
      >
        {vector.getLabel()}
      </span>
      <div
        style={{
          height: 0,
          borderTop: "2px #222 " + (vector.isCommon() ? "solid" : "dashed"),
          width: vector.getMagnitude(),
        }}
      ></div>
    </div>
  );
};
