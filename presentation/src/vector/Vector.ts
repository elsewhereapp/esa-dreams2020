export default class Vector {
    protected label: string;

    //number from 0 to 360
    protected direction: number;

    //number from 0 to 100 in px
    protected magnitude: number;

    //whether it's a shared concept across both documents
    protected common: boolean;

    constructor(label: string, direction: number, magnitude: number, common: boolean) {
        this.label = label;
        this.direction = direction;
        this.magnitude = magnitude;
        this.common = common;
    }

    getLabel(): string {
        return this.label;
    }

    getDirection(): number {
        return this.direction;
    }

    getMagnitude(): number {
        return this.magnitude;
    }

    setMagnitude(magnitude: number): void {
        this.magnitude = magnitude;
    }

    isCommon(): boolean {
        return this.common;
    }
}