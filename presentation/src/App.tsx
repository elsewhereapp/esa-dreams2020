import React, { useState } from "react";
import "./App.css";
import { ScaleLoader } from "react-spinners";
import SimilarityResults from "./SimilarityResults";
import axios from "axios";

function App() {
  const [loading, setLoading] = useState(false);
  const [results, setResults] = useState<any>(null);
  const [dream, setDream] = useState<string>("");

  const analyzeDream = async () => {
    setResults(null);
    setLoading(true);
    try {
      const results = await axios.get("http://35.246.55.164:8080/presentation", {
        params: {
          dream,
          concepts: 1000,
          records: 1,
        },
        responseType: "json",
      });
      console.log(results.data);
      setResults(results.data);
    } catch (e) {
      console.error(e);
    } finally {
      setLoading(false);
    }
  };

  const clearResults = () => {
    setDream("");
    setLoading(false);
    setResults(null);
  };

  return (
    <div className="app">
      <div className="header">
        <h2 className="authors">SHELDON JUNCKER, Dan Kennedy & Gez Quinn</h2>
        <h1>Exploring Dreams with Wikipedia-based Semantic Analysis</h1>
        <h2>CALCULATING DREAM-NEWS SIMILARITY FOR DREAMS 2020</h2>
      </div>
      <div className="body">
        <div className="documents">
          <div className="boxes">
            <div className="box">
              <h3>Dream Report</h3>
              <textarea
                className="dream-entry"
                value={dream}
                onChange={e => setDream(e.target.value)}
                placeholder="Enter your dream..."
              ></textarea>
            </div>
            <div className="box">
              <h3>News Report</h3>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {results && (
                  <textarea className="dream-entry" disabled={true}>
                    {results.articleText}
                  </textarea>
                )}
                {loading && <ScaleLoader color={"#777"} height={40} />}
              </div>
            </div>
          </div>
          <div className="explore-buttons">
            <button className="explore" onClick={analyzeDream}>
              Find similar news
            </button>
            <button className="clear" onClick={clearResults}>
              Clear
            </button>
          </div>
        </div>
        <div className="vectors">
          <div className="loader-wrapper">{loading && <ScaleLoader color={"#777"} height={40} />}</div>
          <div>{results !== null && <SimilarityResults results={results} />}</div>
        </div>
      </div>
    </div>
  );
}

export default App;
