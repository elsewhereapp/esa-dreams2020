#!/bin/bash
java -Djdbc.drivers=org.postgresql.Driver -DtotalEntitySizeLimit=2147480000 -Djdk.xml.totalEntitySizeLimit=2147480000 -Xmx8G -jar target/dreams2020-1.0-jar-with-dependencies.jar "$@"